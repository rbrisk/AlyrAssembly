#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;
use Pod::Usage;

use IO::Uncompress::AnyUncompress;

my ($man, $help) = (0) x 2;
my ($renameTrans, $renameMRna, $padN) = (0) x 3;
my ($pathIn, $pathOut);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"i=s"    => \$pathIn,
		"o=s"    => \$pathOut,
		"t"      => \$renameTrans,
		"m"      => \$renameMRna,
		"p=i"    => \$padN,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;


sub makeId {
	my ($F, $prevParent, $prevId) = @_;
	$F->[8] =~ /Parent=([^;]+)/;
	my $parentId = $1;
	my $t = $F->[2];
	if ( ! defined($prevParent->{$t}) || $parentId ne $prevParent->{$t}) {
		$prevId->{$t} = 0;
		$prevParent->{$t} = $parentId;
	}
	$prevId->{$t}++;
	my $i = $prevId->{$t};
	if ($F->[8] =~ /ID=[^;]+/) {
		$F->[8] =~ s/ID=([^;]+)/ID=$1$i/;
	} else {
		$F->[8] .= ';' unless $F->[8] =~ /;$/;
		$F->[8] .= "ID=${parentId}." . $F->[2] . $i . ';';
	}
}


my $fh = new IO::Uncompress::AnyUncompress($pathIn);
my %prevParent;
my %prevId;
while (<$fh>) {
	# No commments
	next if (/^#/);

	chomp;
	my @F = split("\t", $_, 9);
	if ($padN > 0) {
		$F[-1] =~ s/g(\d+)/sprintf("g%0${padN}d",$1)/ge;
	}

	if ($F[2] =~ /^(intron|exon|cds)$/i) {
		makeId(\@F, \%prevParent, \%prevId);
	}

	if ($renameTrans && $F[2] eq 'mRNA') {
		$F[2] = 'transcript';
	} elsif ($renameMRna && $F[2] eq 'transcript') {
		$F[2] = 'mRNA';
	}

	print join("\t", @F), "\n";
}
close($fh);



__END__

=encoding utf8

=head1 NAME

18_fixAugustusGff.pl - fixes problems in GFF file produced by AUGUSTUS.

=head1 SYNOPSIS

18_fixAugustusGff.pl -i output.gtf | xz - > annotation_fixed.gff.xz

=head1 OPTIONS

=over 8

=item B<-i>
GFF/GTF file produced by AUGUSTUS with C<--gff3=on> parameter.

=item B<-o>
(Optional) Output GTF/GFF file. If not specified, prints to STDOUT.

=item B<-p>
(Optional) If specified, the numbers in gene ids will be padded to have p number of digits.

=item B<-t>
(Optional) If specified, 'mRNA' type will be renamed to 'transcript'

=item B<-m>
(Optional) If specified, 'transcript' type will be renamed to 'mRNA'

=back

=head1 DESCRIPTION

The script Fixes the following problems in the output file produced by AUGUSTUS with C<--gff3=on>.

=over 8

=item
1. Makes cds IDs unique by adding ordinal numbers (within mRNA)

=item
2. Create unique IDs for introns and exons

=item
3. Remove comments (they are redundant in many cases)

=item
4. Pad gene numbers with zeros to facilitate gene sorting.

=item
5. (Optional) Rename 'mRNA' type to 'transcript' or vice versa

=back

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut


