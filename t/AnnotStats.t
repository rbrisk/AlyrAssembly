use strict;
use warnings;

use Test::Exception;
use Test::More tests => 15;

BEGIN {
	use_ok("AnnotStats");
}

require_ok("AnnotStats");

# Arguments: $genes - an array of gene hashes. Gene has two keys s (strand) and t (transcript
# array).  Transcript array has [utr5, utr3] as the first element. The rest are CDS coordinate
# arrays consisting of beg and end positions. For example, the following record has two genes. The
# first gene is on the + strand and has two transcript. The first transcript has no UTR regions and
# two exons (CDS). The second transcript has both UTR regions and three exons (CDS). The order of
# UTRs is the same regardless of the strand. Note that for genes on '-' strand utr5 (first value) is
# added to the last exon end position.
# my $s = geneArrToString([
#     { s => '+', t => [
#           [[0,0], [30,40], [60,75]],
#           [[2,3], [10,20], [30,40], [60,75]] 
#        ] },
#     { s => '-', t => [ [[3,1], [80,95], [110,130]] ] }
#  ]);
#
# $lastGeneId - counter for gene ids indicating the last used gene id
# $mRna - flag specifying whether 'mRNA' type should be used to denote transcripts. By default, 
# transcripts have 'transcript' type.
#
# *** This method cannot create models with exons that are entirely untranslated. ***
sub geneArrToStr {
	my ($genes, $lastGidRef, $mRna) = @_;

	my $geneCnt;
	my @src = qw/ s1 src /;
	my $gffStr = "";
	my $tLbl = $mRna ? 'mRNA' : 'transcript';
	for my $i (0..$#$genes) {
		++$$lastGidRef;
		my $g = $genes->[$i];
		my $strand = $g->{'s'};
		my @qual = ('.', $strand, '.');
		my $gId = "g" . $$lastGidRef;
		# Beg of the first exon of the first transcript
		my ($gBeg, $gEnd) = ($g->{'t'}[0][1][0], 0);
		my $childStr = "";

		for my $j (0..$#{$g->{'t'}}) {
			my $t = $g->{'t'}[$j];
			my $tId = join('.', $gId, "t$j");
			my $tAttr = "ID=$tId;Parent=$gId";
			my $utr5 = $t->[0][0];
			my $utr3 = $t->[0][1];
			my $beg = $t->[1][0];
			my $end = $t->[-1][1];
			$beg -= $strand eq '+' ? $utr5 : $utr3;
			$end += $strand eq '+' ? $utr3 : $utr5;
			$gBeg = $beg if $beg < $gBeg;
			$gEnd = $end if $end > $gEnd;
			$childStr .= join("\t", @src, $tLbl, $beg, $end, @qual, $tAttr) . "\n";
			for my $k (1..$#$t) {
				my $eId = join('.', $tId, "e$k");
				my $eAttr = "ID=$eId;Parent=$tId";
				my $cId = join('.', $tId, "c$k");
				my $cAttr = "ID=$cId;Parent=$tId";
				my ($beg, $end) = @{$t->[$k]};
				$childStr .= join("\t", @src, 'CDS', $beg, $end, @qual, $cAttr) . "\n";
				if ($k == 1) {
					$beg -= $strand eq '+' ? $utr5 : $utr3;
				} elsif ($k == $#$t) {
					$end += $strand eq '+' ? $utr3 : $utr5;
				}
				$childStr .= join("\t", @src, 'exon', $beg, $end, @qual, $eAttr) . "\n";
			}
		}
		$gffStr .= join("\t", @src, 'gene', $gBeg, $gEnd, @qual, "ID=$gId") . "\n";
		$gffStr .= $childStr;
	}
	return $gffStr;
}


sub getPre {
	my ($lastGeneIdRef) = @_;
	my $genes = [
		{ s => '+', t => [
				[[0,0], [30,40], [60, 75]],
				[[3,6], [10,21], [30, 40], [60,75]] ] },
		{ s => '-', t => [
				[[0,9], [101, 120], [151, 190]] ] }
	];
	return geneArrToStr($genes, $lastGeneIdRef);
}


sub getPost {
	my ($lastGeneIdRef) = @_;
	my $genes = [
		{ s => '+', t => [
				[[0,0], [10021,10040], [10071,10120], [10256,10295]] ] },
		{ s => '-', t => [
				[[2,4], [10343, 10442], [10501,10700]] ] }
	];
	return geneArrToStr($genes, $lastGeneIdRef);
}


sub initObj {
	my %args = @_;
	my $lastGeneIdRef = $args{'-lastGeneIdRef'};
	my $genes = $args{'-genes'};
	my $woPre = $args{'-woPre'};
	my $woPost= $args{'-woPost'};

	my $gffStr = "";
	$gffStr .= getPre($lastGeneIdRef) unless $woPre;
	$gffStr .= geneArrToStr($genes, $lastGeneIdRef) if $genes;
	$gffStr .= getPost($lastGeneIdRef) unless $woPost;
	open(my $fh, "<", \$gffStr) or die("Something is wrong with stringstream. $!");
	my $obj = AnnotStats->new();
	$obj->loadData($fh);
	$fh->close();
	return $obj;
}


subtest "DB loading" => sub {
	plan tests => 6;
	my $lastGeneId = 0;
	my $obj = initObj(-lastGeneIdRef => \$lastGeneId);
	my @genes = $obj->{'db'}->features(-type => 'gene');
	my @transcripts = $obj->{'db'}->features(-type => 'transcript');
	my @exons = $obj->{'db'}->features(-type => 'exon');
	my @cds = $obj->{'db'}->features(-type => 'CDS');
	cmp_ok(scalar(@genes), '==', 4, "Loaded genes");
	cmp_ok(scalar(@transcripts), '==', 5, "Loaded transcripts");
	cmp_ok(scalar(@exons), '==', 12, "Loaded exons");
	cmp_ok(scalar(@cds), '==', 12, "Loaded 12 CDS");
	my $gId = $genes[0]->primary_id;
	my ($gName) = $genes[0]->get_tag_values('load_id');
	my %cdsGeneIds;
	my %cdsGeneNames;
	for my $t ($genes[0]->get_SeqFeatures('transcript')) {
		for my $c ($t->get_SeqFeatures('CDS')) {
			my ($cdsGId) = $c->get_tag_values('geneId');
			my ($cdsGName) = $c->get_tag_values('geneName');
			$cdsGeneIds{$cdsGId} = 1;
			$cdsGeneNames{$cdsGName} = 1;
		}
	}
	is_deeply(\%cdsGeneIds, { $gId => 1 }, 'Set parental gene ids');
	is_deeply(\%cdsGeneNames, { $gName => 1}, 'Set parental gene names');
};


subtest "Transcript labels" => sub {
	plan tests => 3;
	my $lastGeneId = 0;
	my $genes = [
			{ s => '-', t => [ [[0,0], [1301, 1600]] ] } 
		];
	my $gffStr = geneArrToStr($genes, \$lastGeneId, 1);
	open(my $fh, "<", \$gffStr) or die("Something is wrong with stringstream. $!");
	my $obj = AnnotStats->new();
	$obj->loadData($fh);
	is($obj->{'mRnaLbl'}, 'mRNA', 'Detect when label is mRNA');
	$fh->close();
	
	$obj = initObj(-genes => $genes, -lastGeneIdRef => \$lastGeneId);
	is($obj->{'mRnaLbl'}, 'transcript', 'Detect when label is transcript');

	$gffStr = geneArrToStr([{s=>'-', t=>[ [[0,0], [1101,1130]] ]}], \$lastGeneId);
	$gffStr .= geneArrToStr($genes, \$lastGeneId, 1);
	open($fh, "<", \$gffStr) or die("Something is wrong with stringstream. $!");
	$obj = AnnotStats->new();
	dies_ok { $obj->loadData($fh) } 'Die if both transcript and mRNA labels are present';
	$fh->close();
};


subtest 'Simple counts' => sub {
	plan tests => 5;
	my $lastGeneId = 0;
	my $genes = [
			{ s => '+', t => [ [[30,9], [1301, 1600], [1631, 1930], [2011,2310], [2411, 2440]] ] }
		];
	my $obj = initObj(-genes => $genes, -lastGeneIdRef => \$lastGeneId);
	cmp_ok($obj->countGenes, '==', 5, "Returns correct gene count");
	cmp_ok($obj->countTranscripts, '==', 6, "Returns correct transcript count");
	cmp_ok($obj->countExons, '==', 16, "Returns correct exon count");
	cmp_ok($obj->countCds, '==', 16, "Returns correct CDS count");

	$obj = AnnotStats->new();
	my $gffStr = geneArrToStr($genes, \$lastGeneId, 1);
	open(my $fh, '<', \$gffStr) or die("Something is wrong with stringstream. $!");
	$obj->loadData($fh);
	cmp_ok($obj->countTranscripts, '==', 1, "Returns correct mRNA count");
	$fh->close();
};


subtest 'Gene overlap without CDS overlap' => sub {
	plan tests => 3;
	my $lastGeneId = 0;
	my $genes = [
			{ s => '+', t => [ [[0, 30], [1101, 1160], [1201, 1260]] ] },
			{ s => '+', t => [ [[60, 0], [1261, 1330], [1421, 1521]] ] } ];
	my $obj = initObj( -genes => $genes, -lastGeneIdRef => \$lastGeneId );
	is_deeply($obj->findGeneOverlaps(), { 'g3' => ['g4'], 'g4' => ['g3'] }, 'Gene overlap');
	is_deeply($obj->findCdsOverlaps(), { }, 'No CDS overlap');
	is_deeply($obj->findGenesInUtr(), { }, 'No genes within UTRs');
	#cmp_ok($obj->getGeneOverlapCnt(), '==', 1, 'Gene overlap');
	#cmp_ok($obj->getCdsOverlapCnt(), '==', 0, 'No CDS overlap');
	#cmp_ok($obj->getGeneInUtrCnt(), '==', 0, 'No genes within UTRs');
};


subtest 'CDS overlap' => sub {
	plan tests => 3;
	my $lastGeneId = 0;
	my $genes = [
			{ s => '-', t => [ [[0, 0], [1101, 1160], [1201, 1260]] ] },
			{ s => '-', t => [ [[0, 0], [1231, 1330], [1421, 1521]] ] } ];
	my $obj = initObj( -genes => $genes, -lastGeneIdRef => \$lastGeneId );
	is_deeply($obj->findGeneOverlaps(), { 'g3' => ['g4'], 'g4' => ['g3'] }, 'Gene overlap');
	is_deeply($obj->findCdsOverlaps(),  { 'g3' => ['g4'], 'g4' => ['g3'] }, 'CDS overlap');
	is_deeply($obj->findGenesInUtr(), { }, 'No genes within UTRs');
#	cmp_ok($obj->getGeneOverlapCnt(), '==', 1, 'Gene overlap');
#	cmp_ok($obj->getCdsOverlapCnt(), '==', 1, 'CDS overlap');
#	cmp_ok($obj->getGeneInUtrCnt(), '==', 0, 'No genes within UTRs');
};


subtest 'Count only one overlap per gene pair' => sub {
	plan tests => 3;
	my $lastGeneId = 0;
	my $genes = [
			{ s => '+', t => [ [[0, 0], [1101, 1160], [1201, 1260], [1291,1350], [1401, 1430]] ] },
			{ s => '+', t => [ [[0, 0], [1231, 1330], [1421, 1521]] ] } ];
	my $obj = initObj( -genes => $genes, -lastGeneIdRef => \$lastGeneId );
	is_deeply($obj->findGeneOverlaps(), { 'g3' => ['g4'], 'g4' => ['g3'] }, 'Gene overlap');
	is_deeply($obj->findCdsOverlaps(),  { 'g3' => ['g4'], 'g4' => ['g3'] }, 'CDS overlap');
	is_deeply($obj->findGenesInUtr(), { }, 'No genes within UTRs');
#	cmp_ok($obj->getGeneOverlapCnt(), '==', 1, 'Gene overlap');
#	cmp_ok($obj->getCdsOverlapCnt(), '==', 1, 'CDS overlap');
#	cmp_ok($obj->getGeneInUtrCnt(), '==', 0, 'No genes within UTRs');
};


subtest 'Three gene overlap' => sub {
	plan tests => 3;
	my $lastGeneId = 0;
	my $genes = [
			{ s => '+', t => [ [[0, 0], [1101, 1160], [1201, 1260], [1291,1350]] ] },
			{ s => '+', t => [ [[0, 0], [1231, 1330], [1401, 1430]] ] },
			{ s => '+', t => [ [[0, 0], [1421, 1521], [1700, 1790]] ] } ];
	my $obj = initObj( -genes => $genes, -lastGeneIdRef => \$lastGeneId );
	is_deeply(
			$obj->findGeneOverlaps(), 
			{ 'g3' => ['g4'], 'g4' => ['g3','g5'], 'g5' => ['g4'] }, 
			'Gene overlap');
	is_deeply(
			$obj->findCdsOverlaps(),
			{ 'g3' => ['g4'], 'g4' => ['g3','g5'], 'g5' => ['g4'] }, 
			'CDS overlap');
	is_deeply($obj->findGenesInUtr(), { }, 'No genes within UTRs');
};


subtest 'Multiple transcripts without CDS overlap' => sub {
	plan tests => 3;
	my $lastGeneId = 0;
	my $genes = [
			{ s => '+', t => [ 
					[[0, 30], [1101, 1160], [1201, 1260], [1351, 1410]],
					[[0, 30], [1101, 1160], [1351, 1410]] ] },
			{ s => '+', t => [ 
					[[60, 0], [1261, 1330], [1421, 1521]],
					[[60, 0], [1261, 1330], [1611, 1640], [1801, 1890]] ] } ];
	my $obj = initObj( -genes => $genes, -lastGeneIdRef => \$lastGeneId );
	is_deeply($obj->findGeneOverlaps(), { 'g3' => ['g4'], 'g4' => ['g3'] }, 'Gene overlap');
	is_deeply($obj->findCdsOverlaps(), { }, 'No CDS overlap');
	is_deeply($obj->findGenesInUtr(),  { }, 'No genes within UTRs');
#	cmp_ok($obj->getGeneOverlapCnt(), '==', 1, 'Gene overlap');
#	cmp_ok($obj->getCdsOverlapCnt(), '==', 0, 'No CDS overlap');
#	cmp_ok($obj->getGeneInUtrCnt(), '==', 0, 'No genes within UTRs');
};


subtest 'Multiple transcripts with CDS overlap' => sub {
	plan tests => 3;
	my $lastGeneId = 0;
	my $genes = [
			{ s => '-', t => [ 
					[[0, 0], [1101, 1160], [1201, 1260], [1351, 1440]],
					[[0, 0], [1101, 1160], [1351, 1440]] ] },
			{ s => '-', t => [ 
					[[0, 0], [1231, 1330], [1421, 1521]],
					[[0, 0], [1231, 1330], [1611, 1640], [1801, 1890]] ] } ];
	my $obj = initObj( -genes => $genes, -lastGeneIdRef => \$lastGeneId );
	is_deeply($obj->findGeneOverlaps(), { 'g3' => ['g4'], 'g4' => ['g3'] }, 'Gene overlap');
	is_deeply($obj->findCdsOverlaps(),  { 'g3' => ['g4'], 'g4' => ['g3'] }, 'CDS overlap');
	is_deeply($obj->findGenesInUtr(), { }, 'No genes within UTRs');
#	cmp_ok($obj->getGeneOverlapCnt(), '==', 1, 'Gene overlap');
#	cmp_ok($obj->getCdsOverlapCnt(), '==', 1, 'No CDS overlap');
#	cmp_ok($obj->getGeneInUtrCnt(), '==', 0, 'No genes within UTRs');
};


subtest 'Genes in UTR' => sub {
	plan tests => 3;
	my $lastGeneId = 0;
	my $genes = [
			{ s => '-', t => [
					[[0, 0], [1101, 1160], [1201, 1260]] ] },
			{ s => '+', t => [
					[[300, 0], [1401, 1700], [1801, 1890]] ] }
		];
	my $obj = initObj ( -genes => $genes, -lastGeneIdRef => \$lastGeneId );
	is_deeply($obj->findGeneOverlaps(), { 'g3' => ['g4'], 'g4' => ['g3'] }, 'Gene overlap');
	is_deeply($obj->findCdsOverlaps(),  { }, 'CDS overlap');
	is_deeply($obj->findGenesInUtr(),   { 'g3' => 'g4' }, 'Gene within UTR');
#	cmp_ok($obj->getGeneOverlapCnt(), '==', 1, 'Gene overlap');
#	cmp_ok($obj->getCdsOverlapCnt(), '==', 0, 'No CDS overlap');
#	cmp_ok($obj->getGeneInUtrCnt(), '==', 1, 'Gene in UTR');
};


subtest 'Ignore gene in UTR if CDS overlap' => sub {
	plan tests => 3;
	my $lastGeneId = 0;
	my $genes = [
			{ s => '-', t => [
					[[0, 0], [1101, 1160], [1201, 1260]] ] },
			{ s => '+', t => [
					[[100, 0], [1201, 1700], [1801, 1890]] ] }
		];
	my $obj = initObj( -genes => $genes, -lastGeneIdRef => \$lastGeneId );
	is_deeply($obj->findGeneOverlaps(), { 'g3' => ['g4'], 'g4' => ['g3'] }, 'Gene overlap');
	is_deeply($obj->findCdsOverlaps(),  { 'g3' => ['g4'], 'g4' => ['g3'] }, 'CDS overlap');
	is_deeply($obj->findGenesInUtr(),   { }, 'Gene within UTR is not reported');
#	cmp_ok($obj->getGeneOverlapCnt(), '==', 1, 'Gene overlap');
#	cmp_ok($obj->getCdsOverlapCnt(), '==', 1, 'CDS overlap');
#	cmp_ok($obj->getGeneInUtrCnt(), '==', 0, 'Gene in UTR not counted');
};


subtest "UTR histograms" => sub {
	# Cases
	# 1. Simple counts
	# 2. Negative strand
	# 3. Right bound open
	# 4. Multiple transcripts
	# 5. Longest transcript flag
	plan tests => 5;
	my $lastGeneId = 0;

	# Simple counts
	my $genes = [
			{ s => '+', t => [ [[ 0,  0], [1101, 1160], [1201, 1260]] ] },
			{ s => '+', t => [ [[10,  0], [1415, 1504], [1612, 1701], [1824, 1973]] ] },
			{ s => '+', t => [ [[25, 60], [2037, 2156], [2254, 2553]] ] }
		];
	my $obj = initObj(-genes => $genes, -woPre => 1, -woPost => 1, -lastGeneIdRef => \$lastGeneId);
	is_deeply(
			$obj->utrLengthHist(20, 3),
			{ -utr5 => [2, 1, 0], -utr3 => [2, 0, 1] },
			'Simple counts'
		);

	# Negative strand
	$lastGeneId = 0;
	$genes = [
			{ s => '+', t => [ [[ 0,  0], [1101, 1160], [1201, 1260]] ] },
			{ s => '+', t => [ [[10,  0], [1415, 1504], [1612, 1701], [1824, 1973]] ] },
			{ s => '-', t => [ [[60, 25], [2037, 2156], [2254, 2553]] ] }
		];
	$obj = initObj(-genes => $genes, -woPre => 1, -woPost => 1, -lastGeneIdRef => \$lastGeneId);
	is_deeply(
			$obj->utrLengthHist(20, 3),
			{ -utr5 => [2, 0, 1], -utr3 => [2, 1, 0] },
			'Negative strand'
		);

	# Right bound open
	$lastGeneId = 0;
	$genes = [
			{ s => '+', t => [ [[ 0,  0], [1101, 1160], [1201, 1260]] ] },
			{ s => '-', t => [ [[20, 19], [1415, 1504], [1612, 1701], [1824, 1973]] ] },
			{ s => '+', t => [ [[41, 40], [2037, 2156], [2254, 2553]] ] }
		];
	$obj = initObj(-genes => $genes, -woPre => 1, -woPost => 1, -lastGeneIdRef => \$lastGeneId);
	is_deeply(
			$obj->utrLengthHist(20, 3),
			{ -utr5 => [1, 1, 1], -utr3 => [2, 0, 1] },
			'Right bound open'
		);

	# Multiple transcripts
	$lastGeneId = 0;
	$genes = [
			{ s => '+', t => [ 
					[[ 0,  0], [1101, 1160], [1201, 1260], [1274, 1363]],
					[[ 0,  0], [1101, 1160], [1274, 1363]] 
				] },
			{ s => '+', t => [ 
					[[10, 41], [1415, 1504], [1612, 1701], [1824, 1973]],
					[[99, 19], [1415, 1504], [1824, 1973]] 
				] },
			{ s => '+', t => [ 
					[[25, 60], [2037, 2156], [2254, 2553]] 
				] }
		];
	$obj = initObj(-genes => $genes, -woPre => 1, -woPost => 1, -lastGeneIdRef => \$lastGeneId);
	is_deeply(
			$obj->utrLengthHist(20, 3),
			{ -utr5 => [3, 1, 1], -utr3 => [3, 0, 2] },
			'Multiple transcripts'
		);

	# Longest transcript flag
	$lastGeneId = 0;
	$genes = [
			{ s => '+', t => [ 
					[[ 0,  0], [1101, 1160], [1201, 1260], [1274, 1363]],
					[[ 0,  0], [1101, 1160], [1274, 1363]],
				] },
			{ s => '+', t => [ 
					[[99, 19], [1415, 1504], [1824, 1973]],
					[[10, 41], [1415, 1504], [1612, 1701], [1824, 1973]],
				] },
			{ s => '+', t => [ 
					[[25, 60], [2037, 2156], [2254, 2553]] 
				] }
		];
	$obj = initObj(-genes => $genes, -woPre => 1, -woPost => 1, -lastGeneIdRef => \$lastGeneId);
	is_deeply(
			$obj->utrLengthHist(20, 3, 1),
			{ -utr5 => [2, 1, 0], -utr3 => [1, 0, 2] },
			'Longest transcript flag'
		);

};


subtest "UTR lengths" => sub {
	# Cases (copy of UTR histogram test)
	# 1. Simple
	# 2. Negative strand
	# 3. Multiple transcripts
	# 4. Longest transcript flag
	plan tests => 4;
	my $lastGeneId = 0;

	my @expectedPre  = ( [qw/ g1 1 0 0 /], [qw/ g1 2 3 6 /], [qw/ g2 1 0 9/] );
	# Assumes three genes inserted in-between
	my @expectedPost = ( [qw/ g6 1 0 0 /], [qw/ g7 1 2 4 /] );

	# Simple counts
	my $genes = [
			{ s => '+', t => [ [[ 0,  0], [1101, 1160], [1201, 1260]] ] },
			{ s => '+', t => [ [[10,  0], [1415, 1504], [1612, 1701], [1824, 1973]] ] },
			{ s => '+', t => [ [[25, 60], [2037, 2156], [2254, 2553]] ] }
		];
	my $obj = initObj(-genes => $genes, -lastGeneIdRef => \$lastGeneId);
	is_deeply(
			$obj->utrLengths(),
			[ @expectedPre, [qw/ g3 1 0 0/], [qw/ g4 1 10 0 /], [qw/ g5 1 25 60 /], @expectedPost ],
			'Simple'
		);

	# Negative strand
	$lastGeneId = 0;
	$genes = [
			{ s => '+', t => [ [[ 0,  0], [1101, 1160], [1201, 1260]] ] },
			{ s => '+', t => [ [[10,  0], [1415, 1504], [1612, 1701], [1824, 1973]] ] },
			{ s => '-', t => [ [[60, 25], [2037, 2156], [2254, 2553]] ] }
		];
	$obj = initObj(-genes => $genes, -lastGeneIdRef => \$lastGeneId);
	is_deeply(
			$obj->utrLengths(),
			[ @expectedPre, [qw/ g3 1 0 0 /], [qw/ g4 1 10 0 /], [qw/ g5 1 60 25 /], @expectedPost ],
			'Negative strand'
		);

	# Multiple transcripts
	$lastGeneId = 0;
	$genes = [
			{ s => '+', t => [ 
					[[ 0,  0], [1101, 1160], [1201, 1260], [1274, 1363]],
					[[ 0,  0], [1101, 1160], [1274, 1363]] 
				] },
			{ s => '+', t => [ 
					[[10, 41], [1415, 1504], [1612, 1701], [1824, 1973]],
					[[99, 19], [1415, 1504], [1824, 1973]] 
				] },
			{ s => '+', t => [ 
					[[25, 60], [2037, 2156], [2254, 2553]] 
				] }
		];
	$obj = initObj(-genes => $genes, -lastGeneIdRef => \$lastGeneId);
	is_deeply(
			$obj->utrLengths(),
			[ 
				@expectedPre, 
				[qw/ g3 1  0  0 /], [qw/ g3 2  0  0 /],
				[qw/ g4 1 10 41 /], [qw/ g4 2 99 19 /],
				[qw/ g5 1 25 60 /], 
				@expectedPost,
			],
			'Multiple transcripts'
		);

	# Longest transcript flag
	$lastGeneId = 0;
	$genes = [
			{ s => '+', t => [ 
					[[ 0,  0], [1101, 1160], [1201, 1260], [1274, 1363]],
					[[ 0,  0], [1101, 1160], [1274, 1363]],
				] },
			{ s => '+', t => [ 
					[[99, 19], [1415, 1504], [1824, 1973]],
					[[10, 41], [1415, 1504], [1612, 1701], [1824, 1973]],
				] },
			{ s => '+', t => [ 
					[[25, 60], [2037, 2156], [2254, 2553]] 
				] }
		];
	$obj = initObj(-genes => $genes, -lastGeneIdRef => \$lastGeneId);
	is_deeply(
			$obj->utrLengths(1),
			[ 
					[qw/ g1 1 3 6 /], [qw/ g2 1 0 9/],
					[qw/ g3 1 0 0 /], [qw/ g4 1 10 41 /], [qw/ g5 1 25 60 /], 
					@expectedPost 
				],
			'Longest transcript flag'
		);

};

