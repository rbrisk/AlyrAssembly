#!/usr/bin/env perl

use strict;
use warnings;

use Getopt::Long;
use Log::Log4perl qw(:easy);
use Pod::Usage;
use File::Basename;
use File::Path qw( make_path );
use File::Spec;
use IO::Uncompress::AnyUncompress;

use lib File::Spec->join(dirname($0), "lib");
use AnnotComp;

my $man = 0;
my $help = 0;
my $logLvlStr = "i";
my ($pathA, $pathB, $labelA, $labelB, $outFmt, $pathFused, $pathSplit);
my ($fmtPlain, $fmtTextile, $fmtLatex) = ('plain', 'textile', 'latex');
my %outFormats = ( $fmtPlain => 1, $fmtTextile => 1, $fmtLatex => 1 );

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"v=s"    => \$logLvlStr,
		"1=s"    => \$pathA,
		"2=s"    => \$pathB,
		"a=s"    => \$labelA,
		"b=s"    => \$labelB,
		"f=s"    => \$outFmt,
		"r=s"    => \$pathFused,
		"s=s"    => \$pathSplit,
	) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});

sub canWrite {
	my ($path, $label, $isReq) = @_;
	if ( ! defined($path) ) {
		die("$label is required") unless ! $isReq;
	} else {
		my $dir = dirname($path);
		make_path($dir) unless -d $dir;
		die("Unable to write to the $label path '$path'") unless -w $dir;
	}
	return 1;
}


die("Annotation 1 path is required") unless $pathA;
die("Annotation 2 path is required") unless $pathB;
$labelA = "Annot1" unless $labelA;
$labelB = "Annot2" unless $labelB;
$outFmt = $fmtPlain unless defined($outFmt) && defined($outFormats{$outFmt});
canWrite($pathFused, "fused gene", 0);
canWrite($pathSplit, "split gene", 0);


sub writeGeneList {
	my ($path, $genes, $lblA, $lblB) = @_;
	open(FOUT, ">", $path) or die("Unable to write to ($path). $!");
	print join("\t", $lblA, $lblB, 'Complete'), "\n";
	my $full = $genes->{'-full'};
	my $partial = $genes->{'-partial'};
	my @allKeys = ( keys(%$full), keys(%$partial) );
	for my $k ( sort(@allKeys) ) {
		my ($v, $isFull);
		if (defined($full->{$k})) {
			$v = $full->{$k};
			$isFull = 1;
		} else {
			$v = $partial->{$k};
			$isFull = 0;
		}
		print FOUT join("\t", $k, join(", ", @$v), $isFull), "\n";
	}
}


DEBUG("Loading annotation $labelA");
my $annotComp = AnnotComp->new();
my $fhA = IO::Uncompress::AnyUncompress->new($pathA);
my $countsA = $annotComp->loadData($fhA, $labelA);
$fhA->close();
DEBUG("Loading annotation $labelB");
my $fhB = IO::Uncompress::AnyUncompress->new($pathB);
my $countsB = $annotComp->loadData($fhB, $labelB);
$fhB->close();

DEBUG("Counting matching genes");
my $geneCounts = $annotComp->countGeneMatches($labelA, $labelB);
my ($gCompleteN, $gPartialN) = @$geneCounts{qw/ -completeN -mRnaN /};

DEBUG("Counting matching transcripts");
my $mRnaCounts = $annotComp->countMRnaMatches($labelA, $labelB);
my @mRnaCountFields = qw/ -completeN -diffUtrN -diffUtr3N -diffUtr5N /;
my ($mCompleteN, $mUtrN, $mUtr5N, $mUtr3N) = @$mRnaCounts{@mRnaCountFields};

DEBUG("Looking for fused genes");
# gene id is the key, list of gene ids is the value
my $fusedGenes = $annotComp->findFusedGenes($labelA, $labelB);
my ($fusedFull, $fusedPartial) = @$fusedGenes{qw/ -full -partial /};
my $fusedFullN = scalar(keys(%$fusedFull));
my $fusedPartialN = scalar(keys(%$fusedPartial));

DEBUG("Looking for split genes");
my $splitGenes = $annotComp->findFusedGenes($labelB, $labelA);
my ($splitFull, $splitPartial) = @$splitGenes{qw/ -full -partial /};
my $splitFullN = scalar(keys(%$splitFull));
my $splitPartialN = scalar(keys(%$splitPartial));

DEBUG("Printing results");
my ($genesA, $mRnaA, $cdsA) = @$countsA{qw/ -geneN -mRnaN -cdsN /};
my ($genesB, $mRnaB, $cdsB) = @$countsB{qw/ -geneN -mRnaN -cdsN /};
# The number of CDS parts should always be the largest number
my $numLen = $cdsA > $cdsB ? length($cdsA) : length($cdsB);
my ($prefix, $suffix, $delim) = ('', "\n", ' ');
if ($outFmt eq $fmtTextile) {
	$prefix = '| ';
	$suffix = " |\n";
	$delim = ' | ';
} elsif ($outFmt eq $fmtLatex) {
	$suffix = "\\\\\n";
	$delim = ' & ';
}
my $mTotal = $mCompleteN + $mUtrN + $mUtr5N + $mUtr3N;
my $rowFmt = $prefix . join($delim, "%${numLen}s", "%${numLen}s", "%s") . $suffix;
printf $rowFmt, $labelA, $labelB, '';
printf $rowFmt, $genesA, $genesB, 'genes';
printf $rowFmt, $mRnaA, $mRnaB, 'mRNAs';
printf $rowFmt, $cdsA, $cdsB, 'CDS parts';
printf $rowFmt, $gCompleteN, $gCompleteN, 'complete gene matches';
printf $rowFmt, $gPartialN, $gPartialN, 'partial gene matches (1 or more mRNAs)';
printf $rowFmt, $mCompleteN, $mCompleteN, 'complete mRNA matches';
printf $rowFmt, $mUtrN, $mUtrN, 'partial mRNA matches, diff. UTRs';
printf $rowFmt, $mUtr5N, $mUtr5N, "partial mRNA matches, diff. 5' UTR";
printf $rowFmt, $mUtr3N, $mUtr3N, "partial mRNA matches, diff. 3' UTR";
printf $rowFmt, $mTotal, $mTotal, 'total mRNA matches';
printf $rowFmt, $fusedFullN, $splitFullN, 'fused genes, complete';
printf $rowFmt, $fusedPartialN, $splitPartialN, 'fused genes, partial';
#printf $rowFmt, $splitGeneN, $fusedGeneN, 'split genes';


writeGeneList($pathFused, $fusedGenes, $labelA, $labelB) if ($pathFused);
writeGeneList($pathSplit, $splitGenes, $labelB, $labelA) if ($pathSplit);


__END__

=head1 NAME

AnnotComp.pl - compares two annotations

=head1 SYNOPSIS

AnnotComp.pl -1 annotA.gff.xz -2 AnnotB.gff -a AnnotA -b AnnotB -f textile -r fused.txt -s split.txt

=head1 OPTIONS

=over 8

=item B<-1>

Path to the first annotation file in GFF format. The file should be well-formed. In particular, all
entries including CDS should have unique IDs (Augustus fails to provide unique IDs for CDS by
default.) The file may be compressed.

=item B<-2>

Path to the second annotation file in GFF format. 

=item B<-a>

Label for the first annotation. It is recommended to use a short label without spaces.

=item B<-b>

Label for the second annotation. It is recommended to use a short label without spaces.

=item B<-f>

(Optional) Output format ('plain', 'textile', 'latex'). The output is not well-formed and some
adjustments would be needed. Default: 'plain'

=item B<-r>

(Optional) Path for writing the list of fused genes, i.e. genes in the first annotation that combine
CDS parts from two or more genes in the second annotation. The list is not written unless the path is
specified.

=item B<-s>

(Optional) Path for writing the list of split genes, i.e. genes in the second annotation that combine
CDS parts from two or more genes in the first annotation. The list is not written unless the path is
specified.

=item B<-v>

(Optional) Log level ('d' - debug, 'i' - info, 'w' - warn, 'e' - error, 'f' - fatal). Default: 'i'

=back

=head1 DESCRIPTION

This is the command line interface for the AnnotComp module. It calls the module's functions to
compare two annotations. Both annotations should be generated against the same reference genome. The
annotations have to be in GFF format and well-formed. In particular, all entries should have unique
IDs (By default, Augustus does not provide unique IDs for CDS parts.)

The script reports the number of genes, transcripts, CDS parts, complete gene model matches, partial
gene model matches (whenever 1 or more transcripts match but not all of them), complete transcript
matches, partial transcript matches, fused genes, and split genes.

Fused genes are the genes in the first annotation that combine CDS parts from two or more genes from
the second annotation. Split genes are the opposite, i.e. the genes in the second annotation that
combine CDS parts from two or more genes from the first annotation. Any fusion (or split) event is
counted only once.

If a format other than plain is specified, the corresponding delimiters are added between the fields.

The script can also report all split and fused genes. The output is written to the specified files
and has two columns. The first column lists the incorporating gene while the second column has a
coma-separated list of the fused genes. The output format is the same for both fused genes and split
genes. However, in the fused gene file, the incorporating gene comes from the first annotation. In
the split gene file, the incorporating gene comes from the second annotation.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut
