#!/usr/bin/env perl

use strict;
use warnings;

use Getopt::Long;
use Pod::Usage;
use Bio::SeqIO;
use Number::Format qw( format_number );


my $man = 0;
my $help = 0;
my $minLen = 0;
my ($pathAssembly, $expLen);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"i=s"    => \$pathAssembly,
		"e=i"    => \$expLen,
		"m=i"    => \$minLen,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Assembly path is required") unless $pathAssembly;
die("Expected genome length is required") unless $expLen;


sub calcNLx {
	my ($sortedLengths, $thres) = @_;
	my $i = 0;
	my $cumLen = $sortedLengths->[0];
	while ($cumLen < $thres && $i < @$sortedLengths - 1) {
		$cumLen += $sortedLengths->[++$i];
	}
	# If the cumulative length is below the threshold, return 0
	my $N = $cumLen >= $thres ? $sortedLengths->[$i] : 0;
	return ($N, $i + 1);
}

sub countNucByType {
	my ($seqs) = @_;
	my %nucs;
	my $total = 0;
	while (my ($k, $s) = each(%$seqs)) {
		$total += $s->length;
		for my $n (split('', uc($s->seq))) {
			$nucs{$n} = 0 if ! defined($nucs{$n});
			$nucs{$n}++;
		}
	}
	my $acgt = $nucs{'A'} + $nucs{'C'} + $nucs{'G'} + $nucs{'T'};
	my $ambig = $total - $acgt - $nucs{'N'};
	my $gc = ($nucs{'C'} + $nucs{'G'}) / $acgt * 100;
	return ($acgt, $nucs{'N'}, $ambig, $gc);
}




# Expected length is in Mbp
$expLen *= 1e+6;

my %seqs;
my @allLengths;
my $assemblyLen = 0;
my $reader = Bio::SeqIO->new( -file => $pathAssembly, -format => 'fasta' );
while (my $seq = $reader->next_seq()) {
	if ($seq->length >= $minLen) {
		$seqs{$seq->display_id} = $seq;
		push(@allLengths, $seq->length);
		$assemblyLen += $seq->length;
	}
}
$reader->close();

my @sortedLengths = sort { $b <=> $a } @allLengths;

my ($N25, $L25) = calcNLx(\@sortedLengths, 0.25 * $assemblyLen);
my ($N50, $L50) = calcNLx(\@sortedLengths, 0.50 * $assemblyLen);
my ($N75, $L75) = calcNLx(\@sortedLengths, 0.75 * $assemblyLen);

my ($NG25, $LG25) = calcNLx(\@sortedLengths, 0.25 * $expLen);
my ($NG50, $LG50) = calcNLx(\@sortedLengths, 0.50 * $expLen);
my ($NG75, $LG75) = calcNLx(\@sortedLengths, 0.75 * $expLen);

my ($acgtN, $missN, $ambigN, $gcPcnt) = countNucByType(\%seqs);

print "Expected length, bp: " . format_number($expLen, 0) . "\n";
print "Total length, bp: " . format_number($assemblyLen, 0) . "\n";
print sprintf("ACGT, bp: %s (%0.2f%%)\n", format_number($acgtN), $acgtN / $assemblyLen * 100);
print sprintf("Missing, bp: %s (%0.2f%%)\n", format_number($missN), $missN / $assemblyLen * 100);
print sprintf("Ambiguous, bp: %s (%0.2f%%)\n", format_number($ambigN), $ambigN / $assemblyLen * 100);
print sprintf("GC content, %%: %0.2f\n", $gcPcnt);
print "Count: " . format_number(scalar(@sortedLengths), 0) . "\n";
print "Longest, bp: " . format_number($sortedLengths[0], 0) . "\n";
print "Shortest, bp: " . $sortedLengths[-1]. "\n";
print "N/L25: " . join(" / ", format_number($N25, 0), format_number($L25, 0)) . "\n";
print "N/L50: " . join(" / ", format_number($N50, 0), format_number($L50, 0)) . "\n";
print "N/L75: " . join(" / ", format_number($N75, 0), format_number($L75, 0)) . "\n";
print "N/LG25: " . join(" / ", format_number($NG25, 0), format_number($LG25, 0)) . "\n";
print "N/LG50: " . join(" / ", format_number($NG50, 0), format_number($LG50, 0)) . "\n";
print "N/LG75: " . join(" / ", format_number($NG75, 0), format_number($LG75, 0)) . "\n";


__END__

=encoding utf8

=head1 NAME

00_calcAssemblyStats.pl - calculates assembly statistics including N50, length, missing data, etc.

=head1 SYNOPSIS

00_calcAssemblyStats.pl -i assembly.fa -e 250 -m 200

=head1 OPTIONS

=over 8

=item B<-i>
Genome assembly in fasta format

=item B<-e>
Expected nuclear genome size in Mbp

=item B<-m>
(Optional) length threshold for sequence filtering. Shorter sequences are ignored. Default: 0

=back

=head1 DESCRIPTION

Calculates various assembly statistics including total length, missing data, GC content, min/max
sequence length, N50, L50, NG50, LG50, etc. Nx values count all nucleotides including missing and
ambiguous values. For N/LGx values, the NGx is set to 0 whenever the total length of the assembly is
below C<x * assemblyLen / 100>.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut
