#!/usr/bin/env perl

use strict;
use warnings;

#use Data::Dumper;
#$Data::Dumper::Sortkeys = 1;

use Getopt::Long;
use Pod::Usage;
use Log::Log4perl qw(:easy);
use Bio::SeqIO;

my $man = 0;
my $help = 0;
my $logLvlStr = 'i';
my $nThres = 50;
my ($pathPrimary, $pathAssembly, $pathOut);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"p=s"    => \$pathPrimary,
		"a=s"    => \$pathAssembly,
		"o=s"    => \$pathOut,
		"n=i"    => \$nThres,
		"v=s"    => \$logLvlStr,
	) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Primary path is required") unless $pathPrimary;
die("Assembly path is required") unless $pathAssembly;
die("Output path is required") unless $pathOut;

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});

INFO("Loading assembly sequence");
my %genome;
my $reader = Bio::SeqIO->new( -file => $pathAssembly, -format => 'fasta' );
while (my $seq = $reader->next_seq()) {
	$genome{$seq->display_id()} = $seq;
}
$reader->close();

INFO("Finding cutting sites");
open(FIN, "<", $pathPrimary) or die("Unable to read the primary file ($pathPrimary). $!");
<FIN>;
my @loci;
my $spliceSiteN = 0;
my @spliceSites;
my $cutSiteN = 0;
while(<FIN>) {
	chomp;
	my @F = split("\t", $_);
	#my ($hitScaf, $hitBeg, $hitEnd, $isSynt, $geneN) = @F[0..2,8,9];
	my ($hitScaf, $hitBeg, $hitEnd) = @F[0..2];
	my $prevLocus = $loci[-1];
	push(@loci, \@F);
	next unless (@loci > 1 && $prevLocus->[0] eq $hitScaf);

	$spliceSiteN++;

	my $beg = $prevLocus->[2];
	my $end = $hitBeg;

	next if ($beg > $end);

	my $seq = $genome{$hitScaf}->subseq($beg, $end);
	my @seqArr = split(//, $seq);
	my $n = 0;
	for (my $i = $#seqArr; $i >= 0; $i--) {
		if ($seqArr[$i] eq 'N') {
			$n++;
		} else {
			$n = 0;
		}
		if ($n >= $nThres) {
			# Position of the first N
			my $spliceEnd = $end - ($#seqArr - $i) + ($nThres - 1);
			while ($i >= 0 && $seqArr[$i] eq 'N') {
				$i--;
			}
			my $spliceBeg = $end - ($#seqArr - $i) + 1;
			push(@spliceSites, [$hitScaf, $spliceBeg, $spliceEnd]);
			$cutSiteN++;
			last;
		}
	}
}
close(FIN);


INFO("Writing the output");
open(FOUT, ">", $pathOut) or die("Unable to write to ($pathOut). $!");
print FOUT join("\t", qw/ Scaffold Beg End /), "\n";
for my $s (@spliceSites) {
	print FOUT join("\t", @$s), "\n";
}
close(FOUT);

INFO("$cutSiteN out of $spliceSiteN can be cut");

__END__

=head1 NAME

05_findCutSites.pl - finds sites between primary hits where the scaffold sequence can be cut

=head1 SYNOPSIS

05_findCutSites.pl -p primary.txt -a assembly.fa -o cutsites.txt

=head1 OPTIONS

=over 8

=item B<-p>

Path to the file listing the primary syntenic regions. The file should have the following columns:
scaffold, beg, end, gene number, reference scaffold, reference beg, and reference end.

=item B<-a>

Path to the assembly file in fasta format.

=item B<-o>

Path to the output file that will list the scaffold name, beg and end positions. Begin and end
positions are the coordinates of the first and last N of the cutting site.

=item B<-n>

Minimum number of Ns required for a cutting site.

=back


=head1 DESCRIPTION

Finds sites where incorrectly spliced scaffolds can be cut. Those sites should contains at least the
specified number of Ns. The output will include the scaffold name and the coordinates of the first
and the last N of the cutting site.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut
