#!/usr/bin/env perl

use strict;
use warnings;

use Getopt::Long;
use Log::Log4perl qw(:easy);
use Pod::Usage;
use File::Basename;
use File::Path qw( make_path );
use File::Spec;
use IO::Uncompress::AnyUncompress;

use lib File::Spec->join(dirname($0), "lib");
use AnnotStats;

my $man = 0;
my $help = 0;
my $logLvlStr = "i";
my $utrLenStep = 100;
my $utrLenBinN = 10;
my $utrLenUseLongestT = 0;
my ($pathAnnot, $pathOverlap, $pathUtrLen, $outFmt, $quickMode);
my ($fmtPlain, $fmtTextile, $fmtLatex) = ('plain', 'textile', 'latex');
my %outFormats = ( $fmtPlain => 1, $fmtTextile => 1, $fmtLatex => 1 );

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"v=s"    => \$logLvlStr,
		"a=s"    => \$pathAnnot,
		"o=s"    => \$pathOverlap,
		"u=s"    => \$pathUtrLen,
		"s=i"    => \$utrLenStep,
		"n=i"    => \$utrLenBinN,
		"l"      => \$utrLenUseLongestT,
		"f=s"    => \$outFmt,
		"q"      => \$quickMode,
	) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});

sub canWrite {
	my ($path, $label, $isReq) = @_;
	if ( ! defined($path) ) {
		die("$label is required") unless ! $isReq;
	} else {
		my $dir = dirname($path);
		make_path($dir) unless -d $dir;
		die("Unable to write to the $label path '$path'") unless -w $dir;
	}
	return 1;
}


die("Annotation path is required") unless $pathAnnot;
canWrite($pathOverlap, "overlap", 0);
canWrite($pathUtrLen, "UTR length", 0);
die("UTR length step should be between 1 and 5000") unless ($utrLenStep > 0 && $utrLenStep <= 5000);
die("UTR length bin number should be between 1 and 1000") 
		unless ($utrLenBinN > 0 && $utrLenBinN < 1000);
$outFmt = $fmtPlain unless defined($outFmt) && defined($outFormats{$outFmt});


DEBUG("Loading the data");
my $annotStats = AnnotStats->new();
my $fh = IO::Uncompress::AnyUncompress->new($pathAnnot);
$annotStats->loadData($fh);
$fh->close();

# Quick stats
DEBUG("Counting genes");
my $geneN = $annotStats->countGenes;
DEBUG("Counting transcripts");
my $mRnaN = $annotStats->countTranscripts;
DEBUG("Counting CDS");
my $cdsN  = $annotStats->countCds;
DEBUG("Counting exons");
my $exonN = $annotStats->countExons;

# Slow stats
my ($overlapGenes, $overlapCds, $genesInUtr);
my ($geneOverlapN, $cdsOverlapN, $geneInUtrN) = ('NA') x 3;
if ( ! $quickMode) {
	if ($pathOverlap) {
		open(FOLP, ">", $pathOverlap) or die("Unable to write to ($pathOverlap). $!");
		print FOLP join("\t", qw/ Type GeneA GeneB /), "\n";
	}

	DEBUG("Seeking gene overlaps");
	$overlapGenes = $annotStats->findGeneOverlaps;
	$geneOverlapN = 0;
	for my $k ( sort( keys(%$overlapGenes) ) ) {
		my $genes = $overlapGenes->{$k};
		$geneOverlapN += scalar(@$genes);
		for my $g (@$genes) {
			print FOLP join("\t", 'GOL', $k, $g), "\n" if $pathOverlap;
		}
	}
	# Each gene pair is counted twice
	$geneOverlapN /= 2;

	DEBUG("Seeking CDS overlaps");
	$overlapCds = $annotStats->findCdsOverlaps;
	$cdsOverlapN = 0;
	for my $k ( sort( keys(%$overlapCds) ) ) {
		my $genes = $overlapCds->{$k};
		$cdsOverlapN += scalar(@$genes);
		for my $g (@$genes) {
			print FOLP join("\t", 'COL', $k, $g), "\n" if $pathOverlap;
		}
	}
	# Each pair is counted twice
	$cdsOverlapN /= 2;

	DEBUG("Seekeing genes in UTRs");
	$genesInUtr = $annotStats->findGenesInUtr;
	$geneInUtrN = scalar(keys(%$genesInUtr));
	for my $k ( sort( keys(%$genesInUtr) ) ) {
		print FOLP join("\t", 'GIU', $k, $genesInUtr->{$k}), "\n" if $pathOverlap;
	}

	close(FOLP) if $pathOverlap;
}

DEBUG("Generating UTR length histogram");
my $utrLenHist = $annotStats->utrLengthHist($utrLenStep, $utrLenBinN, $utrLenUseLongestT);

if ($pathUtrLen) {
	DEBUG("Getting UTR lengths");
	my $utrLengths = $annotStats->utrLengths;
	open(FUTR, ">", $pathUtrLen) or die("Unable to write to '$pathUtrLen'. $!");
	print FUTR join("\t", qw/ Gene mRNA UTR5 UTR3 /), "\n";
	for my $r (@$utrLengths) {
		print FUTR join("\t", @$r), "\n";
	}
	close(FUTR);
}

DEBUG("Printing results");
if ($outFmt eq $fmtPlain) {
	# The number of exons is always the largest number
	my $numLen = length($exonN);
	my $rowFmt = "% ${numLen}s %s\n";
	printf $rowFmt, $geneN, 'genes';
	printf $rowFmt, $mRnaN, 'transcripts';
	printf $rowFmt, $cdsN, 'CDS parts';
	printf $rowFmt, $exonN, 'exons';
	printf $rowFmt, $geneOverlapN, 'overlapping gene models';
	printf $rowFmt, $cdsOverlapN, 'overlapping CDS parts';
	printf $rowFmt, $geneInUtrN, 'genes in UTR of other genes';
	print "\n";
	print "UTR length distribution\n";
	print join("\t", qw/ Range UTR5 UTR3 /), "\n";
	for my $i (0..($utrLenBinN - 1)) {
		my $rangeStr = sprintf("%3s - %3s", $i * $utrLenStep, ($i + 1) * $utrLenStep - 1);
		print join("\t", $rangeStr, $utrLenHist->{'-utr5'}[$i], $utrLenHist->{'-utr3'}[$i]), "\n";
	}
	my $rangeStr = "> " . $utrLenBinN * $utrLenStep . "    ";
	print join("\t", $rangeStr, $utrLenHist->{'-utr5'}[-1], $utrLenHist->{'-utr3'}[-1]), "\n";
} else {
	my @header = qw/ Genes Transcr CDSParts Exons OverlapGene OverlapCDS GeneInUTR /;
	$header[3] = 'Exons '; # In case there are more than 99k, it will align properly
	my ($delim, $prefix, $suffix) = (' ', '', '');
	if ($outFmt eq $fmtTextile) {
		$prefix = '| ';
		$suffix = " |\n";
		$delim = ' | '
	} elsif ($outFmt eq $fmtLatex) {
		$suffix = " \\\\\n";
		$delim = ' & ';
	}
	print $prefix, join($delim, @header), $suffix;
	my @out = ( $geneN, $mRnaN, $cdsN, $exonN, $geneOverlapN, $cdsOverlapN, $geneInUtrN );
	for my $i (0..$#out) {
		my $fFmt = sprintf("%%%ds", length($header[$i]));
		$out[$i] = sprintf($fFmt, $out[$i]);
	}
	print $prefix, join($delim, @out), $suffix;
	print "\n";
	print "UTR length distribution\n\n";
	my @utrHeader;
	for my $i (0..($utrLenBinN - 2)) {
		push(@utrHeader, sprintf("%d - %d", $i * $utrLenStep, ($i + 1) * $utrLenStep - 1));
	}
	push(@utrHeader, "> " . ($utrLenBinN - 1) * $utrLenStep);
	my @utr5 = @{$utrLenHist->{'-utr5'}};
	my @utr3 = @{$utrLenHist->{'-utr3'}};
	for my $i (0..$#utrHeader) {
		my $fFmt = sprintf("%%%ds", length($utrHeader[$i]));
		$utr5[$i] = sprintf($fFmt, $utr5[$i]);
		$utr3[$i] = sprintf($fFmt, $utr3[$i]);
	}
	print $prefix, join($delim, 'Type', @utrHeader), $suffix;
	print $prefix, join($delim, 'UTR5', @utr5), $suffix;
	print $prefix, join($delim, 'UTR3', @utr3), $suffix;
}


__END__

=encoding utf8

=head1 NAME

AnnotStats.pl - generates various annotation statistics for the specified GFF file.

=head1 SYNOPSIS

AnnotStats.pl -a annotation.gff.xz -f textile -o overlaps.txt

=head1 OPTIONS

=over 8

=item B<-a>

Path to the annotation file in GFF format. The file should be well-formed. In particular, all entries
including CDS should have unique IDs (Augustus fails to provide unique IDs for CDS by default.) The
file may be compressed.

=item B<-o>

(Optional) Path to the output file for writing the overlaps.

=item B<-u>

(Optional) Path to the output file for writing UTR lengths.

=item B<-s>

(Optional) Step size for generating UTR length histogram. Default: 100

=item B<-n>

(Optional) Number of bins for generating UTR length histogram. Default: 10

=item B<-l>

(Optional) If specified, the UTR length histogram will consider the longest transcripts per gene.

=item B<-f>

(Optional) Output format ('plain', 'textile', 'latex'). In plain format, the output is vertical. In
textile and latex formats, the output is horizontal. As a result, large histogram bin number may be
problematic. The output is not well-formed and some adjustments would be needed. Default: 'plain'

=item B<-q>

(Optional) If specified, all time-consuming calculations (overlap counts) are omitted.

=item B<-v>

(Optional) Log level ('d' - debug, 'i' - info, 'w' - warn, 'e' - error, 'f' - fatal). Default: 'i'


=back

=head1 DESCRIPTION

This is the command line interface for the AnnotStats module. It calls the module's functions to
generate various statistics for an annotation in GFF format. The output includes gene count,
transcript count, exon count, CDS part count, gene overlap count, and UTR length histogram (text
only).

If the output path is specified with C<-o>, the script will write all overlapping genes to that path.
The first column is the type of overlap: GOL (gene overlap), COL (CDS overlap), or GIU (gene in UTR).
The second and third columns contain gene names. For the first two types, the pairs are reciprocical,
i.e. there is (x,y) record for each (y,x) record. The GIU type contains unique pairs where the first
gene is the container and the second gene is one that is located in a UTR of the first gene.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut
