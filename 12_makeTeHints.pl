#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;
use Pod::Usage;

our $delim = "\t";
my $help = 0;
my $man = 0;
my $sourceTag = "RM";
my ($pathIn, $range);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"i=s"    => \$pathIn,
		"r=s"    => \$range,
		"s=s"    => \$sourceTag,
	) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;


die("Input file is required") unless $pathIn;
die("Input file ($pathIn) does not exist") unless -f $pathIn;

my ($src, $beg, $end, $hasBounds);
if ($range) {
	die("Invalid range format ($range)") unless ($range =~ /^([^:]+):(\d+)\.\.(\d+)$/);
	$src = $1;
	$beg = $2;
	$end = $3;
	die("Invalid range: Start > Stop") unless ($beg < $end);
	$hasBounds = 1;
}
	

open(FIN, "<", $pathIn) or die("Unable to read the input file ($pathIn). $!");
while (<FIN>) {
	next if /^#/;
	chomp;
	my @fields = split($delim);
	if ($hasBounds) {
		# At least one end should be within the range
		next unless ($fields[0] eq $src && (
				($fields[3] >= $beg && $fields[3] <= $end) ||
				($fields[4] >= $beg && $fields[4] <= $end)));
		$fields[0] = join("_", $fields[0], $beg, $end);
		# Convert into relative coordinates
		$fields[3] = $fields[3] - $beg + 1;
		$fields[4] = $fields[4] - $beg + 1;
		# Truncate if one of the ends fall outside of the range
		$fields[3] = 1 if ($fields[3] < 0);
		$fields[4] = $end if ($fields[4] > $end);
	}
	$fields[2] = "nonexonpart";
	$fields[8] .= ";source=" . $sourceTag;
	print join($delim, @fields), "\n";
}
close(FIN);


__END__

=head1 NAME

12_makeTeHints.pl - converts Censor or RepeatMasker GFF file into Augustus hints file.

=head1 SYNOPSIS

12_makeTeHints.pl -i censor.gff

=head1 OPTIONS

=over 8

=item B<-i>

Input file in GFF format that contains Censor or RepeatMasker results

=item B<-r>

(Optional) Range to be selected from the file. Format: Chr3:1001..2000

=item B<-s>

(Optional) "source" attribute tag for Augustus. Default: RM

=back  
 
=head1 DESCRIPTION

Converts Censor or RepeatMasker GFF file into format that can be used for
Augustus hints. If the range is specified, the Source tag (chromosome) is updated to include the
range and the absolute coordinates are changed to relative coordinates. If the range is specified
and a TE spans the range boundary, it will be included but its location truncates to fit the range.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

